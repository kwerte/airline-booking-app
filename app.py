from flask import Flask
from flask import render_template, request, redirect
from flask_sqlalchemy import sqlalchemy
from sqlalchemy import create_engine


engine = create_engine('postgresql://postgres:1998068Wenxin!@localhost/airline')
#replace above with your own username, password, and database

app = Flask(__name__)
app.debug = True
globalemail = ""

@app.route('/')
def Welcome():
    return render_template('index.html', title='Airline Booking App')
@app.route('/about')
def about():
    return render_template('about.html', title='About page')
@app.route('/register',methods=['GET', 'POST'])
def register():
    return render_template('register.html', title = 'Sign Up')
@app.route('/create_acct', methods = ['POST'])
def create_acct():
    email = request.form['email']
    global globalemail
    globalemail = email
    name = request.form['name']
    homeairport = request.form['homeairport']
    query = "INSERT INTO customer(email_address, name, home_iata) VALUES ('" + email + "', '" + name + "', '" + homeairport + "')"
    engine.execute(query)
    return render_template("index.html")
@app.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('login.html', title='Sign In')
@app.route('/login_process', methods=['POST'])
def login_process():
    email=request.form['email']
    query="SELECT * FROM customer WHERE email_address = " + "'" + email + "'"
    result=engine.execute(query)
    result=[dict(r) for r in result]
    if len(result) <= 0:
        return redirect('http://localhost:5000/login')
    else:
        return render_template("show.html")
@app.route('/addPayment')
def add_payment():
    return render_template('addPayment.html')
@app.route("/processPayment", methods = ["POST"])
def processPayment():
    cardnumber = request.form['card_number']
    address = request.form['card_address']
    email_address = request.form['email']
    card_country = request.form['card_country']
    card_state = request.form['card_state']
    card_city = request.form['card_city']
    query = "INSERT INTO customer_credit_card(card_number, card_street, card_country, card_city, email_address) VALUES (" + cardnumber + ",'" + address + "','" + card_country + "','" + card_city + "','" + email_address + "')"
    engine.execute(query)
    return render_template("show.html")
@app.route('/showPayments')
def show_payments():
    query = "SELECT * FROM customer_credit_card WHERE email_address=" + "'" + globalemail + "'"
    result = engine.execute(query)
    payments = [dict(r) for r in result]
    return render_template("showPayments.html", payments = payments)
@app.route("/modifyPayment")
def modifyPayment():
    return render_template("modifyPayment.html")

@app.route("/processModifyPayment/<column>", methods = ["POST"])
def processModifyPayment(column):
    query = ""
    card_number = request.form['card_number']
    if  column == "card_address":
        card_street = request.form['card_street']
        query = "UPDATE customer_credit_card SET card_street =" + "'" + card_street +  "' WHERE card_number =" + card_number
    elif column == "delete":
        query = "DELETE FROM customer_credit_card WHERE card_number =" +  card_number
    engine.execute(query)
    return render_template("show.html")

@app.route("/addHomeAirport")
def addHomeAirport():
    return render_template("addHomeAirport.html")

@app.route("/processAddHomeAirport", methods = ["POST"])
def processAddHomeAirport():
    iata = request.form['iata']
    name = request.form['name']
    country = request.form['country']
    state = request.form['state']
    query = "INSERT INTO airport(iata, name, country, state) VALUES('" + iata + "','" + name + "','" + country + "','" + state + "')"
    engine.execute(query)
    return render_template("show.html")

@app.route("/bookFlight", methods = ["POST"])
def bookFlight():
    return render_template("bookFlight.html")

@app.route("/processBookFlight", methods = ["POST"])
def processBookFlight():
    card_number = request.form['card_number']
    address = request.form['card_street']
    query = "INSERT INTO booking(card_number) VALUES(" + card_number + ") RETURNING bookingid"
    result = engine.execute(query)
    resId =[dict(r) for r in result][0]['bookid']
    for flight in trip_flight:
        result = engine.execute("SELECT date FROM flight where flight_num = " + flight)
        flightdate = [dict(r) for r in result][0]['date']
        result = engine.execute("SELECT class from prices where flight_num = " + flight)
        fclass = [dict(r) for r in result][0]['class']
        query = "INSERT INTO bookingcontains(bid, flightnumber, flightdate, class) VALUES(" + str(resId) + "," + flight + ",'" + str(flightdate) + "','" + fclass + "')"
        engine.execute(query)
    return render_template("show.html")

@app.route("/manageBookings")
def manageBookings():
    query = "SELECT * from booking"
    result = engine.execute(query)
    bookings = [dict(r) for r in result]
    return render_template("manageBookings.html", bookings = bookings)

@app.route("/deleteBookings", methods = ["POST"])
def deleteBookings():
    id = request.form['bookingid']
    query = "DELETE from booking WHERE bookid=" + id
    result = engine.execute(query)
    query = "SELECT * from booking"
    result = engine.execute(query)
    bookings = [dict(r) for r in result]
    return render_template("manageBookings.html", bookings = bookings)

@app.route("/flightConnections")
def flightConnections():
    return render_template("getFlight.html")

@app.route("/processFlightConnections", methods = ["POST"])
def processFlightConnections():
    departure = request.form['departure']
    destination = request.form['destination']
    price_range = request.form['priceRange']
    num_of_connects = request.form['numofConnect']
    fclass = request.form['class']
    tripTime = request.form['tripTime']
    orderBy = request.form['orderBy']
    query = """
    with recursive translation (flight_num, dep_iata, dest_iata, fnumseq, path, numconnects, price, time)
as
(
    SELECT
        flight_num,
        dep_iata::char,
        dest_iata::char,
        array[flight_num]::varchar[],
        array[dep_iata, dest_iata]::varchar[],
        1::int as numconnects,
        price::numeric(6,2),
        (arrive_time - dep_time)::interval as time
    from flight natural join price
    where dep_iata = 'ORD' and class = ""'" + class + "'""
    union
    select
        s.flight_num,
        s.dep_iata::char,
        s.dest_iata::char,
        (fnumseq || s.flight_num::varchar)::varchar[],
        (path || s.dest_iata::varchar)::varchar[],
        numconnects + 1,
        (t.price::numeric(6,2) + s.price::numeric(6, 2))::numeric(6,2),
        (time + (s.arrive_ltime - s.dep_time)::interval)::interval
    from (flight natural join price) as s
    join translation as t
    on s.dep_iata = t.dest_iata
    where s.dest_iata <> all(t.path) and numconnects< """ + num_of_connects + """
          and ((select date + dep_time from flight where s.flight_num = flight_num) > (select date + arrive_time from flight where t.flight_num = flight_num))
          and t.price::numeric(6, 2) + s.price::numeric(6, 2) < """ + price_range + """
          and s.class = ""'" + class + "'""
          and (select count(flight_num) from flight natural join booking contains where s.flight_num = flight_num) <= s.cap_first+s.cap_econ
          and t.dep_time + (s.arrive_time - s.dep_time)::interval < cast('""" + tripTime + """' as interval)
)
select *
from translation where dest_iata = """ + "'" + departure + "'" + """ order by """ + orderBy + """ asc limit 1;
"""

    result = engine.execute(query)
    flight = [dict(r) for r in result]
    if len(flight)<=0:
        return "No routes availaible for this destination"
    global trip_flight
    trip_flight = flight[0]['fnumseq']
    return render_template("flightConnections.html", flight = flight, departure = departure, destination = destination)



if __name__=='__main__':
    app.run(debug=True)
